Number.prototype.formatMoney = function (c, d, t) {
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d === undefined ? '.' : d,
        t = t === undefined ? '' : t,
        s = n < 0 ? '-' : '',
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + '',
        j = (j = i.length) > 3 ? j % 3 : 0;
    var retval = s + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : '');
    if (retval === '-0.00') {
        retval = '0.00';
    }
    return retval;
};

// converts a price to the specified currency that is used to payment
App.createPaymentCurrencyPrice = function (price, group) {
    var paymentCurrencyPrice = parseFloat(price);
    if (App.settings.paymentCurrency && App.settings.paymentCurrency !== App.settings.currency.code) {
        for (var i = 0; i < App.settings.exchanges.length; i++) {
            var exchange = App.settings.exchanges[i];
            if (exchange.currency === App.settings.paymentCurrency) {
                var defaultRate = exchange.rates[0];
                var individualExchangeRate = App.getIndividualExchangeRate(group, exchange.currency) || defaultRate;
                //console.log("used rate " + individualExchangeRate + " for group " + group  + ", currency " + exchange.currency);
                paymentCurrencyPrice = individualExchangeRate === 0 ? 0 : (paymentCurrencyPrice / individualExchangeRate); 
                return {price: paymentCurrencyPrice.formatMoney(), currency: App.settings.paymentCurrency};
            }
        }
    }
    return {price: paymentCurrencyPrice.formatMoney(), currency: App.settings.currency.code};
};

// search for an individual exchange rate of a currency of a sale group
// returns the exchange rate that match the sale group number and a specified currency
// also checks if the currency rate exists in global exchanges 
// if nothing matches, returns false
App.getIndividualExchangeRate = function (sgNumber, exchangeCurrency) {
    if (App.settings.individualExchanges && App.settings.individualExchanges.length) {
        /*[
            { // individualExchangeRate
                sgNumber: Number,
                currency: String,
                rate: Number
            }
        ];*/
        for (var i = 0; i < App.settings.individualExchanges.length; i++) {
            var ie = App.settings.individualExchanges[i];
            if (ie.sgNumber === sgNumber && ie.currency === exchangeCurrency) {
                for (var j = 0; j < App.settings.exchanges.length; j++) {
                    var exchange = App.settings.exchanges[j]; 
                    if (exchange.currency === exchangeCurrency) {
                        if (exchange.rates.indexOf(ie.rate) >= 0) {
                            return ie.rate;
                        }
                    }
                }
                return false;
            }
        }
        return false;
    }
    return false;
};

