const axios = require('axios');
const btoa = require('btoa'); // note that btoa might not work in React Native
axios.defaults.baseURL = 'https://api.gokasa.cz';

// using Promise
/*
axios.post('/auth', {username: 'demo:demo', password: btoa('demodemo')}).then(function (resp) {
    console.log(resp.data);
    if (!resp.data.isAuthenticated) {
        throw 'not authenticated';
    }
    axios.defaults.headers.cookie = resp.headers['set-cookie'][0];
    
    return axios.get('/api/settings');
}).then(function (resp) {
    console.log(resp.data);
    
    return axios.get('/api/campaigns');
}).then(function (resp) {
    console.log(resp.data);
    
}).catch(function (err) {
    console.error(
        err.response.status, 
        err.response.statusText
    );
});
*/

// using async await
(async function init() {
    try {
        const auth = await axios.post('/auth', {username: 'demo:demo', password: btoa('demodemo')});
        console.log(auth.data);
        axios.defaults.headers.cookie = auth.headers['set-cookie'][0];
        
        const settings = await axios.get('/api/settings');
        console.log(settings.data);
    } catch (error) {
        console.error(error);
    }
})();