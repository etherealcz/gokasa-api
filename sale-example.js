var payload = {
    number: '19010000185',
    date: '2019-08-07T13:11:36.437Z',
    clerk: 'demo',
    items: JSON.stringify([
        { "name": "AKCE: Houska", "ean": "1", "price": "30.00", "quantity": 1, "tax_rate": 15, "group": 1 },
        { "name": "AKCE: Rohlík", "ean": "2", "price": "20.00", "quantity": 1, "tax_rate": 15, "group": 1 },
        { "name": "Chléb Krájený půl", "ean": "7", "price": "20.00", "quantity": 1, "tax_rate": 15, "group": 1 }
    ]),
    tendered: '70.00',
    fik: '',
    discount: '0',
    customerTin: '',
    payMethod: 'cash',
    takeAway: false,
    isFiscal: true,
    foreignPriceTotal: '',
};
